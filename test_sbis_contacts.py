from atf import log
from atf.pytest_core.base.case_ui import TestCaseUI

from pages.contacts import Contacts
from pages.sbis_main import SbisMain


class TestSbisContacts(TestCaseUI):
    """Тестирование раздела 'Контакты'"""

    @classmethod
    def setUpClass(cls):
        cls.main_page = SbisMain(cls.driver)
        cls.contact_page = Contacts(cls.driver)

    def setUp(self):
        log("Открываем главную страницу 'sbis', переходим в раздел 'Контакты'")
        self.main_page.open()
        self.main_page.open_page_via_header("Контакты")

    def tearDown(self):
        self.browser.close_windows_and_alert()

    def test_01_navigate_and_check_tensor_page(self, layout):
        """
        Проверятся переход на страницу 'tensor.ru' и отображение контента.
        """

        log("Проверяем наличие логотипа тензор во вкладке 'Клиентам' и переходим на сайт 'tensor'")
        self.contact_page.check_tensor_logo("Клиентам", layout)
        tensor_page = self.contact_page.go_to_tensor_site()

        log("Проверяем блок 'Сила в людях', переходим по ссылке 'Подробнее'")
        tensor_about = tensor_page.check_about_people_blck(title="Сила в людях", link=True)

        log("Проверяет размер картинок в блоке 'Работаем'")
        tensor_about.check_pictures_working_blck()

    def test_02_region_change(self):
        """
        Проверяется определение региона по-умолчанию и его изменение.
        """
        data = {
            '44 Костромская обл.':
                {1: 'Кострома',
                 2: 'СБИС - Кострома\nул.Лесная, 11, 1 этаж\n+7 4942 64-19-12\n+7 4942 41-92-20\nsbis.ru\ninfo@kostroma.tensor.ru',
                 3: 'Консультант Кострома\nпр-т Мира, 51\n+7 4942 32-74-56\n+7 4942 32-74-50\nveda@consultant44.ru',
                 4: 'Нея',
                 5: 'ИП Турчанинов В.В.\nул. Советская, 31\n+7 49444 2-10-60\ntvv-cw@yandex.ru'},
            '41 Камчатский край':
                {1: 'Петропавловск-Камчатский',
                 2: 'СБИС - Камчатка\nул.Ленинская, 59, оф.202, 205\n+7 4152 21-56-60\n+7 4152 34-01-08\nsbis.ru\ninfo@kamchatka.tensor.ru'}
        }

        my_region, new_region = [*data][0], [*data][1]
        new_region_url = '41-kamchatskij-kraj'

        log('Проверяем регион по-умолчанию и контактные данные')
        self.contact_page.check_region_in_header(my_region[3:])
        self.contact_page.check_region_info(data[my_region])

        log('Меняем регион и проверяем изменения')
        self.contact_page.change_region(new_region)
        self.contact_page.check_region_in_header(new_region[3:])
        self.contact_page.check_region_info(data[new_region])

        log('Проверяем что url и title изменились')
        self.contact_page.check_url(new_region_url)
        self.contact_page.check_title(new_region[3:])
