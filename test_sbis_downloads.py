import os

from atf import log
from atf.pytest_core.base.case_ui import TestCaseUI
from atf.ui import ActionChainsATF, Displayed

from pages.sbis_main import SbisMain
from pages.tensor_download import TensorDownload, check_file_size, delete_file
from pages.libraries.SitesCommon.footer import Footer


class TestSbisDownloads(TestCaseUI):
    """Тестирование раздела 'Контакты'"""

    path_for_downloads = os.path.expanduser(r'~\Downloads')
    link_name = 'Скачать СБИС'
    files = {
        'Сбис Плагин': 'sbisplugin-setup-web.exe'
    }

    @classmethod
    def setUpClass(cls):
        cls.main_page = SbisMain(cls.driver)
        cls.download_page = TensorDownload(cls.driver)
        cls.footer = Footer(cls.driver)

    def setUp(self):
        log("Открываем главную страницу 'sbis'")
        self.main_page.open()
        self.chains = ActionChainsATF(self.driver)
        self.chains.move_to_element(self.footer.footer)
        self.footer.links.should_be(Displayed)
        self.main_page.cookie_agreement_close.click()
        self.footer.select_link(self.link_name)

    def test_01_download_plugin(self):
        """
        Проверяем скачивание Сбис Плагин.
        """

        log('Скачиваем Сбис Плагин и проверяем указанный на сайте размер с фактическим')
        file_size = self.download_page.download_file(
            'СБИС Плагин', 'Веб-установщик', self.files['Сбис Плагин'], self.path_for_downloads)
        check_file_size(self.files['Сбис Плагин'], self.path_for_downloads, file_size)

    def tearDown(self):
        log('Удаляем скачанные файлы')
        for file in self.files.values():
            delete_file(file, self.path_for_downloads)
