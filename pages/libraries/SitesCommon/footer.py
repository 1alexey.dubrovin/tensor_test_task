from atf.ui import *
from selenium.webdriver.common.by import By


@parent_element('.sbisru-Footer')
class Footer(Region):
    """Футер страниц sbis.ru"""

    footer = Element(By.CSS_SELECTOR, '.sbis_ru-container', 'Футер')
    links = CustomList(By.CSS_SELECTOR, 'a.sbisru-Footer__link', 'Список ссылок')

    def select_link(self, name):
        """
        Кликнуть на ссылку из списка.
        :param name: название
        """
        self.footer.should_be(Displayed)
        self.links.item(with_text=name).click()
