from atf import assert_that, equal_to
from atf.exceptions import ElementException
from atf.ui import *
from selenium.webdriver.common.by import By


class TensorMain(Region):
    """Главная страница tensor.ru"""
    url = 'https://tensor.ru/'

    about_people_blck = Element(By.CSS_SELECTOR, '.tensor_ru-Index__block4-content', "Блок сила в людях")
    cookie_agreement_close = Button(
        By.CSS_SELECTOR, '.tensor_ru-CookieAgreement__close', 'Кнопка закрытия окна подтверждения куки')

    def open(self):
        """Открыть страницу"""

        self.browser.open(self.url)
        self.check_page_load_wasaby()

    def check_about_people_blck(self, title, content=None, link=False):
        """
        Проверяет блок о сотрудниках.
        :param title: заголовок
        :param content: содержимое блока
        :param link: ссылку "Подробнее"
        """
        chains = ActionChainsATF(self.driver)
        chains.move_to_element(self.about_people_blck)
        self.about_people_blck.element(By.CSS_SELECTOR, '.tensor_ru-Index__card-title').should_be(ExactText(title))
        if content:
            self.about_people_blck.should_be(ContainsText(content))
        if link:
            from pages.tensor_about import TensorAbout
            about_page = TensorAbout(self.driver)
            link = self.about_people_blck.element(By.CSS_SELECTOR, '.tensor_ru-link')
            link.should_be(ContainsText("Подробнее"))
            try:
                link.click()
            except ElementException:
                self.cookie_agreement_close.click()
                link.click()
            about_page.check_page_load_wasaby()
            assert_that(self.driver.current_url, equal_to(about_page.url),
                        desc=f'Результаты поиска не совпадают с ожидаемыми.')
            return about_page
