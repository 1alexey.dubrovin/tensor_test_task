from atf import *
from atf.ui import *
from controls import ControlsTabsButtons, ControlsListView
from selenium.webdriver.common.by import By


class Contacts(Region):
    """Страница Контакты на сайте sbis.ru"""

    url = 'https://sbis.ru/contacts/'
    base_title = 'Сбис Контакты — '

    tensor_logo = Element(By.CSS_SELECTOR, '.sbisru-Contacts__logo-tensor', "Логотип тензор")
    clients_tab = ControlsTabsButtons(SabyBy.DATA_QA, 'Controls-Tabs__item', "Вкладки разделов контакты")
    region_chooser = Element(By.CSS_SELECTOR, '.sbisru-Contacts .sbis_ru-Region-Chooser__text', "Выбор региона")
    region_info = ControlsListView(locator='.sbisru-Contacts-List__col .controls-ListViewV',
                                   rus_name='Контактная информация')

    # окно выбора региона
    region_cslst = CustomList(By.CSS_SELECTOR, '.sbis_ru-Region-Panel__item', "Регионы")
    close_btn = Button(By.CSS_SELECTOR, '.sbis_ru-Region-Panel__header-close', 'Закрыть')

    def open(self):
        """Открыть страницу"""

        self.browser.open(self.url)
        self.check_page_load_wasaby()

    def check_tensor_logo(self, tab, layout):
        """
        Проверяет отображение логотипа Tensor на указанной вкладке (Клиентам, Партнерам, Разработчикам).
        :param layout: фикстура создания изображений.
        :param tab: вкладка
        """
        self.clients_tab.select(with_text=tab)
        layout.capture('Tensor_Logo', self.tensor_logo)

    def go_to_tensor_site(self):
        """
        Перейти на сайт tensor.ru кликом по баннеру c логотипом
        """
        from pages.tensor_main import TensorMain

        self.tensor_logo.should_be(Displayed)
        self.tensor_logo.click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        tensor_page = TensorMain(self.driver)
        tensor_page.check_page_load_wasaby()

        assert_that(self.driver.current_url, equal_to(tensor_page.url),
                    desc=f'Результаты поиска не совпадают с ожидаемыми.')
        return tensor_page

    def check_region_in_header(self, region_name):
        """
        Проверяет установленный регион.
        :param region_name: название региона
        """
        self.check_page_load_wasaby()
        self.region_chooser.should_be(ExactText(region_name))

    def check_region_info(self, data):
        """
        Проверяет контактные данные в регионе.
        :param data: эталонные данные
        """
        for i in range(1, self.region_info.count_elements + 1):
            assert_that(data[i], equal_to(self.region_info.item(item_number=i).text),
                        'Отображаемые контактные данные не совпадают с эталонными')

    def change_region(self, region_name):
        """
        Изменить регион.
        :param region_name: название
        """
        self.region_chooser.click()
        self.region_cslst.should_be(Displayed)
        self.region_cslst.item(with_text=region_name).click()
        self.region_cslst.should_not_be(Displayed)
        self.region_info.should_be(Displayed)
        self.check_page_load_wasaby()
        # Два метода выше не помогают
        delay(1, 'Ждем перестроение страницы')

    def check_url(self, url):
        """
        Проверяет url страницы.
        :param url: адрес добавляемый к основному
        """
        assert_that(self.url + url, is_in(self.driver.current_url), "URL не соответствует указанному")

    def check_title(self, title):
        """
        Проверяет title вкладки
        :param title: название текущего региона
        """
        assert_that(self.base_title + title, is_in_ignoring_case(self.driver.title.title()),
                    "Title не соответствует указанному")
