import os
import re

from atf import *
from atf.ui import *
from controls import Sbis3ControlTabButtons
from selenium.webdriver.common.by import By


def check_file_size(file_name, path, size):
    """
    Проверяет наличие файла в указанной папке.
    :param file_name: название файла
    :param path: путь к папке
    :param size: размер
    """
    full_path = os.path.join(path, file_name)
    file_info = os.stat(full_path)

    cur_size = str(round(file_info.st_size / 1048576, 2))
    assert_that(size, equal_to(cur_size), "Размер файла не соответствует указанному")


def delete_file(file_name, path):
    """
    Удаляет файл.
    :param file_name: название файла
    :param path: путь к папке
    """
    full_path = os.path.join(path, file_name)
    os.remove(full_path)
    assert_that(file_name, is_not_in(os.listdir(path)), "Файл не удалился")


class TensorDownload(Region):
    """Cтраница tensor.ru/download"""
    url = 'https://sbis.ru/download'

    tabs = Sbis3ControlTabButtons(By.CSS_SELECTOR, '[sbisname="TabButtons"].sbis_ru-VerticalTabs__tabs',
                                  'Разделы скачать')
    downloads = CustomList(By.CSS_SELECTOR, '.sbis_ru-DownloadNew-block', 'Список загрузок')

    def download_file(self, application, file_type, file_name, path):
        """
        Скачать приложение
        :param application: название приложения
        :param file_type: тип файла
        :param file_name: название файла
        :param path: название файла
        :return размер указанный в ссылке
        """
        counter = 0
        self.tabs.select(application)
        file_block = self.downloads.item(contains_text=file_type)
        file_block.should_be(Displayed)
        file_link = file_block.element(By.CSS_SELECTOR, '.sbis_ru-DownloadNew-loadLink')
        file_link.click()
        while file_name not in os.listdir(path):
            delay(2)
            counter += 2
            if counter >= 600:
                raise Exception("Файл не загрузился за 10 мин")
        return re.findall(r'\d+.\d+', file_link.text)[0]
