from atf import assert_that, equal_to
from atf.ui import *
from selenium.webdriver.common.by import By


class TensorAbout(Region):
    """Cтраница tensor.ru/about"""
    url = 'https://tensor.ru/about'

    working_blck = Element(By.CSS_SELECTOR, '.tensor_ru-About__block3', 'Блок "Работаем"')

    def check_pictures_working_blck(self, size=None):
        """
        Проверяет размер картинок в блоке "Работаем".
        Если не передан size - берет размеры 1 и сравнивает с остальными.
        :param size: Размеры изображения
        """
        pictures = self.working_blck.element(By.CSS_SELECTOR, '.tensor_ru-About__block3-image').webelement(return_list=True)
        chains = ActionChainsATF(self.driver)
        chains.move_to_element(self.working_blck)
        if size:
            width, height = size.get('width'), size.get('height')
        else:
            width = pictures[0].get_attribute('width')
            height = pictures[0].get_attribute('height')
        for i in range(len(pictures)):
            cur_width = pictures[i].get_attribute('width')
            cur_height = pictures[i].get_attribute('height')
            assert_that(cur_width, equal_to(width), f'Неверная ширина {i+1} изображения')
            assert_that(cur_height, equal_to(height), f'Неверная высота {i+1} изображения')
