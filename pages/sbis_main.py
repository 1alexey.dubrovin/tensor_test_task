from atf.ui import *
from selenium.webdriver.common.by import By


class SbisMain(Region):
    """Главная страница sbis.ru"""

    cookie_agreement_close = Button(
        By.CSS_SELECTOR, '.sbis_ru-CookieAgreement__close', 'Кнопка закрытия окна подтверждения куки')

    def open(self):
        """Открыть страницу"""

        self.browser.open('https://sbis.ru/')
        self.check_page_load_wasaby()

    def open_page_via_header(self, section_name):
        """
        Открыть страницу используя header
        :param section_name: название вкладки
        """
        section = Element(By.XPATH, f'//*[contains(@class, "sbisru-Header__menu-link") and contains(text(), "{section_name}")]', 'Выбираемый элемент меню', driver=self.driver)
        section.should_be(ExactText(section_name), msg="Текст кнопки не соответствует")
        section.click()
